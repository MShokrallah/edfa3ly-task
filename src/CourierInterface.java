
public interface CourierInterface {

	public void createShipment();
	public void trackShipment();
	
}
