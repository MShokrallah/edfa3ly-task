
public abstract class CourierParent implements CourierInterface {
	private String courierID;
	public abstract CourierParent createCourier(String courierName);
	public String getCourierID() {
		return courierID;
	}
	public void setCourierID(String courierID) {
		this.courierID = courierID;
	}
	public void createShipmentAndGetWaybill(){
		System.out.println("CourierParent::createShipmentAndGetWaybill");
	}
	public void getShipmentTrackingDetails(){
		System.out.println("CourierParent::getShipmentTrackingDetails");
	}

}
