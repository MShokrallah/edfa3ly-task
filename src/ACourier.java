
public class ACourier extends CourierParent {
	
	static
	{
		
		CourierFactory.instance().registerCourier("c1", new ACourier());
	}
	public ACourier() {
			System.out.println("ACourier default constructor");
	}


	public ACourier(String courierID) {
		// TODO Auto-generated constructor stub
		System.out.println("ACourier overload constructor");
		this.setCourierID(courierID);
	}


	

	@Override
	public ACourier createCourier(String name) {
		// TODO Auto-generated method stub
		if(name == null) return new ACourier();
		return new ACourier(name);
	}


	public void createShipmentAndGetWaybill(){
		//if i have a specific implementation 
		super.createShipmentAndGetWaybill();
		System.out.println("ACourier::createShipmentAndGetWaybill");
	}



	public void createShipment() {
		// TODO Auto-generated method stub
		
		System.out.println("ACourier::"+(getCourierID()!=null?getCourierID():"")+"::createShipment");
		createShipmentAndGetWaybill();
		
	}


	public void trackShipment() {
		// TODO Auto-generated method stub
		System.out.println("ACourier::trackShipment");
		getShipmentTrackingDetails();
		
	}

}
