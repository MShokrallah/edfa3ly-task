
public class CCourier extends CourierParent {
	
	static
	{
		
		CourierFactory.instance().registerCourier("c3", new CCourier());
	}
	public CCourier() {
			System.out.println("CCourier default constructor");
	}


	public CCourier(String courierID) {
		// TODO Auto-generated constructor stub
		System.out.println("CCourier overload constructor");
		this.setCourierID(courierID);
	}


	public void createShipment() {
		// TODO Auto-generated method stub
		
		System.out.println("CCourier::"+(getCourierID()!=null?getCourierID():"")+"::createShipment");
		createShipmentAndGetWaybill();
		
	}


	public void trackShipment() {
		// TODO Auto-generated method stub
		System.out.println("CCourier::trackShipment");
		registerNumberToGetTrackingDetails();
		getTrackingDetails();
		
		
	}
	
	private void registerNumberToGetTrackingDetails() {
		// TODO Auto-generated method stub
		System.out.println("CCourier::registerNumberToGetTrackingDetails");
		
	}

	private void getTrackingDetails() {
		// TODO Auto-generated method stub
		System.out.println("CCourier::getTrackingDetails");
		
	}





	@Override
	public CCourier createCourier(String name) {
		// TODO Auto-generated method stub
		if(name == null) return new CCourier();
		return new CCourier(name);
	}



}
