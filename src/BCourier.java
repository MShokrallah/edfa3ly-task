
public class BCourier extends CourierParent {
	
	static
	{
		
		CourierFactory.instance().registerCourier("c2", new BCourier());
	}
	public BCourier() {
			System.out.println("BCourier default constructor");
	}


	public BCourier(String courierID) {
		// TODO Auto-generated constructor stub
		System.out.println("BCourier overload constructor");
		this.setCourierID(courierID);
	}

	
	public void createShipment() {
		// TODO Auto-generated method stub
		
		System.out.println("BCourier::"+(getCourierID()!=null?getCourierID():"")+"::createShipment");
		createShipmentInfo();
		createShipmentInstructions();
		getShipmentWaybill();
		
	}

	public void trackShipment() {
		// TODO Auto-generated method stub
		System.out.println("BCourier::trackShipment");
		getShipmentTrackingDetails();
		
	}
	
	private void createShipmentInfo(){
		System.out.println("BCourier::createShipmentInfo");
	}
	private void createShipmentInstructions(){
		System.out.println("BCourier::createShipmentInstructions");
	}
	/*private void getShipmentTrackingDetails(){
		System.out.println("BCourier::getShipmentTrackingDetails");
	}*/
	private void getShipmentWaybill() {
		// TODO Auto-generated method stub
		System.out.println("BCourier::getShipmentWaybill");
		
	}

	@Override
	public BCourier createCourier(String name) {
		// TODO Auto-generated method stub
		if(name == null) return new BCourier();
		return new BCourier(name);
	}



}
