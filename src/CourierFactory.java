import java.util.HashMap;

public class CourierFactory {

	private HashMap<String, CourierParent> m_RegisteredProducts = new HashMap<String, CourierParent>();

	private static final CourierFactory courierFactory = new CourierFactory();

	private CourierFactory() {
		//loadClass();
	}

	public static CourierFactory instance() {
		// TODO Auto-generated method stub
		return courierFactory;
	}

	public void registerCourier(String courierID, CourierParent c) {
		m_RegisteredProducts.put(courierID, c);
	}

	public CourierParent createCourier(String productID) {
		return ((CourierParent) m_RegisteredProducts.get(productID))
				.createCourier(null);
	}

	public CourierParent createCourier(String productID, String courierName) {
		return ((CourierParent) m_RegisteredProducts.get(productID))
				.createCourier(courierName);
	}

	public  void loadClass() {
		System.out.println("loadClass");
		try {
			// TODO add here classes we add to the project to make an instance of the class which implement the static block that register the class
			Class.forName("ACourier");
			Class.forName("BCourier");
			Class.forName("CCourier");
	
			// Class.forName("DCourier");

		} catch (ClassNotFoundException any) {
			System.out.println("catch");
			any.printStackTrace();
		}
	}

}
